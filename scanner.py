#!/usr/bin/python

import socket

hosts = ['127.0.0.1', '192.168.1.2', '192.168.0.2', '10.10.1.200']

ports = [22, 80, 445, 443, 3389]

s = socket.socket()
print dir(socket)

# Loop and scan.
for host in hosts:
    for port in ports:
        try:
            print 'Attempting socket: '+host+':'+str(port)
            s.connect((host, port))
            s.send('String Information \n')
            banner = s.recv(1024)
            if banner:
                print '[+] Port :'+str(port)+' open: '+banner
            s.close()
        except: pass